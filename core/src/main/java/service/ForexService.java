package service;

import currency.CurrencyState;
import dto.ExchangeRatesResponseAPI;
import dto.Rates;
import enricher.ExchangeRatesMessageEnricher;

public class ForexService {

    public Rates getCurrentRates(CurrencyState baseRate) {
        ExchangeRatesMessageEnricher enricher = new ExchangeRatesMessageEnricher();
        ExchangeRatesResponseAPI response = enricher.enrich(baseRate);
        return response.getRates();
    }
}
