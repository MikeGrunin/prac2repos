package service;

import util.Human;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

@SuppressWarnings("all")
public class HumanService {
    private final Human humanTemplate =
            new Human("rick", "n", "n1", 100, 'm', new Date());

    public void initFirstTask() {
        List<Human> humansList = setUpHumanList();

        executeFistSubTask(humansList);
        executeSecondSubTask(humansList);
        executeThirdSubTask(humansList);
    }

    private void executeFistSubTask(List<Human> humansList) {
        System.out.println("**********************Task1****************************");

        humansList.stream()
                .filter(h -> h.equals(humanTemplate))
                .forEach(System.out::println);
    }

    private void executeSecondSubTask(List<Human> humansList) {
        System.out.println("**********************Task2****************************");

        Predicate<Human> humanPredicateTask2 =
                h -> h.getAge() <= 20 &&
                        (("абвгд".contains(String.valueOf(h.getLastName().charAt(0)))) ||
                                "АБВГД".contains(String.valueOf(h.getLastName().charAt(0))));

        humansList.stream()
                .filter(humanPredicateTask2)
                .forEach(System.out::println);
    }

    private void executeThirdSubTask(List<Human> humansList) {
        Map<Human, Integer> duplicateHumans = new HashMap<>();
        List<Human> moreThan3DuplicatesList = new ArrayList<>();

        System.out.println("**********************Task3****************************");

        Consumer<Human> humanConsumerTask3 = h -> {
            if (duplicateHumans.containsKey(h)) {
                duplicateHumans.put(h, duplicateHumans.get(h) + 1);
            } else {
                duplicateHumans.put(h, 1);
            }
        };

        humansList.stream()
                .forEach(humanConsumerTask3);

        duplicateHumans.entrySet().stream()
                .peek(h -> {
                    if (h.getValue() > 3) moreThan3DuplicatesList.add(h.getKey());
                })
                .sorted((o1, o2) -> o2.getKey().getAge().compareTo(o1.getKey().getAge())) //desc by age
                .forEach(h -> System.out.println(h.getValue() + ":" + h.getKey()));
    }

    private List<Human> setUpHumanList() {
        return List.of(
                new Human("mike", "m", "m1", 22, 'm', new Date()),
                new Human("sam", "s", "s1", 43, 'm', new Date()),
                new Human("sam", "s", "s1", 43, 'm', new Date()),
                new Human("bob", "а", "b1", 12, 'm', new Date()),
                new Human("bob", "а", "b1", 12, 'm', new Date()),
                new Human("bob", "а", "b1", 12, 'm', new Date()),
                new Human("bob", "s", "b1", 12, 'm', new Date()),
                new Human("bob", "а", "b1", 12, 'm', new Date()),
                new Human("rick", "n", "n1", 100, 'm', new Date()),
                new Human("rick", "Г", "n1", 2, 'm', new Date()),
                new Human("john", "Д", "j1", 4, 'm', new Date())
        );
    }
}
