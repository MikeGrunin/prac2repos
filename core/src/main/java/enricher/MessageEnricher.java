package enricher;

import currency.CurrencyState;
import dto.ExchangeRatesResponseAPI;

public interface MessageEnricher {

    ExchangeRatesResponseAPI enrich(CurrencyState currencyState);
}
