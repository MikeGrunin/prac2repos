package enricher;

import com.fasterxml.jackson.databind.ObjectMapper;
import currency.CurrencyState;
import dto.ExchangeRatesResponseAPI;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

public class ExchangeRatesMessageEnricher implements MessageEnricher
{
    Logger log = Logger.getLogger(ExchangeRatesMessageEnricher.class.getName());

    @Override
    public ExchangeRatesResponseAPI enrich(CurrencyState currencyState) {
        ExchangeRatesResponseAPI rates = new ExchangeRatesResponseAPI();
        ObjectMapper mapper = new ObjectMapper();
        try {
            rates = mapper
                    .readValue(
                            new URL("https://api.exchangeratesapi.io/latest?base=" + currencyState.toString()),
                            ExchangeRatesResponseAPI.class);
        } catch (IOException e) {
            log.warning("Incorrect request to Exchange Rates API:" + e.getMessage());
        }
        return rates;
    }
}
