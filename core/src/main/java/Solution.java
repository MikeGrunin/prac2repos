import currency.CurrencyState;
import service.ForexService;
import service.HumanService;

public class Solution {
    public static void main(String[] args) {
        new HumanService().initFirstTask();
        System.out.println(new ForexService().getCurrentRates(CurrencyState.RUB));

    }
}
