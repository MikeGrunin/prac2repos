package util;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.Objects;

@AllArgsConstructor
@Data
public class Human {
    private String firstName;
    private  String lastName;
    private String midName;
    private Integer age;
    private char gender;
    private Date birthDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age.equals(human.age) &&
                gender == human.gender &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(lastName, human.lastName) &&
                Objects.equals(midName, human.midName) &&
                Objects.equals(birthDate, human.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, midName, birthDate);
    }

}
