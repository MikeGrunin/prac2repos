package dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CAD", "HKD", "ISK", "PHP", "DKK", "HUF", "CZK", "GBP",
        "RON", "SEK", "IDR", "INR", "BRL", "RUB", "HRK", "JPY",
        "THB", "CHF", "EUR", "MYR", "BGN", "TRY", "CNY", "NOK",
        "NZD", "ZAR", "USD", "MXN", "SGD", "AUD", "ILS", "KRW", "PLN"
})
public class Rates {
    @JsonProperty("CAD")
    public Double CAD;
    @JsonProperty("HKD")
    public Double HKD;
    @JsonProperty("ISK")
    public Double ISK;
    @JsonProperty("PHP")
    public Double PHP;
    @JsonProperty("DKK")
    public Double DKK;
    @JsonProperty("HUF")
    public Double HUF;
    @JsonProperty("CZK")
    public Double CZK;
    @JsonProperty("GBP")
    public Double GBP;
    @JsonProperty("RON")
    public Double RON;
    @JsonProperty("SEK")
    public Double SEK;
    @JsonProperty("IDR")
    public Double IDR;
    @JsonProperty("INR")
    public Double INR;
    @JsonProperty("BRL")
    public Double BRL;
    @JsonProperty("RUB")
    public Double RUB;
    @JsonProperty("HRK")
    public Double HRK;
    @JsonProperty("JPY")
    public Double JPY;
    @JsonProperty("THB")
    public Double THB;
    @JsonProperty("CHF")
    public Double CHF;
    @JsonProperty("EUR")
    public Double EUR;
    @JsonProperty("MYR")
    public Double MYR;
    @JsonProperty("BGN")
    public Double BGN;
    @JsonProperty("TRY")
    public Double TRY;
    @JsonProperty("CNY")
    public Double CNY;
    @JsonProperty("NOK")
    public Double NOK;
    @JsonProperty("NZD")
    public Double NZD;
    @JsonProperty("ZAR")
    public Double ZAR;
    @JsonProperty("USD")
    public Integer USD;
    @JsonProperty("MXN")
    public Double MXN;
    @JsonProperty("SGD")
    public Double sGD;
    @JsonProperty("AUD")
    public Double AUD;
    @JsonProperty("ILS")
    public Double ILS;
    @JsonProperty("KRW")
    public Double KRW;
    @JsonProperty("PLN")
    public Double PLN;
}
